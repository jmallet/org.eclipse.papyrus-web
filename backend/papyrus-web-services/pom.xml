<?xml version="1.0" encoding="UTF-8"?>
<!--
 Copyright (c) 2022, 2024 CEA LIST, Obeo.
 This program and the accompanying materials
 are made available under the terms of the Eclipse Public License v2.0
 which accompanies this distribution, and is available at
 https://www.eclipse.org/legal/epl-2.0/

 SPDX-License-Identifier: EPL-2.0

 Contributors:
     Obeo - initial API and implementation
-->
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>papyrus-web</groupId>
		<artifactId>papyrus-web-parent</artifactId>
		<version>2024.2.0</version>
		<relativePath>../papyrus-web-parent</relativePath>
	</parent>
	<artifactId>papyrus-web-services</artifactId>
	<name>papyrus-web-services</name>
	<description>Papyrus Web Services</description>


	<dependencies>

		<!-- Internal dependencies -->
		<dependency>
			<groupId>papyrus-web</groupId>
			<artifactId>papyrus-web-representation-builder</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>papyrus-web</groupId>
			<artifactId>papyrus-web-services-api</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>papyrus-web</groupId>
			<artifactId>papyrus-web-persistence</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>papyrus-web</groupId>
			<artifactId>papyrus-web-sirius-contributions</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>papyrus-web</groupId>
			<artifactId>papyrus-web-tests</artifactId>
			<version>${project.version}</version>
			<scope>test</scope>
		</dependency>

		<!-- External Dependencies -->
		<dependency>
			<groupId>org.eclipse.uml2.plugins</groupId>
			<artifactId>org.eclipse.uml2.uml</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-core</artifactId>
		</dependency>
		<dependency>
			<groupId>io.micrometer</groupId>
			<artifactId>micrometer-core</artifactId>
		</dependency>

		<dependency>
			<groupId>org.eclipse.sirius</groupId>
			<artifactId>sirius-components-collaborative-trees</artifactId>
		</dependency>
		<dependency>
			<groupId>org.eclipse.sirius</groupId>
			<artifactId>sirius-web-services-api</artifactId>
		</dependency>
		<dependency>
			<groupId>org.eclipse.sirius</groupId>
			<artifactId>sirius-components-diagrams-layout</artifactId>
			<exclusions>
				<exclusion>
					<groupId>xml-apis</groupId>
					<artifactId>xml-apis</artifactId>
				</exclusion>
				<exclusion>
					<groupId>xml-apis</groupId>
					<artifactId>xml-apis-ext</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.eclipse.sirius</groupId>
			<artifactId>sirius-components-emf</artifactId>
		</dependency>
		<dependency>
			<groupId>org.eclipse.sirius</groupId>
			<artifactId>sirius-components-domain-emf</artifactId>
		</dependency>
		<dependency>
			<groupId>org.eclipse.sirius</groupId>
			<artifactId>sirius-components-view-emf</artifactId>
		</dependency>
		<dependency>
			<groupId>org.eclipse.sirius</groupId>
			<artifactId>sirius-web-persistence</artifactId>
		</dependency>
		<!-- Temporary dependency required for overriding the default model brower representation -->
		<!-- This interface has been created for bug https://gitlab.eclipse.org/eclipse/papyrus/org.eclipse.papyrus-web/-/issues/97. But once https://github.com/eclipse-sirius/sirius-web/issues/2809 is fixed this is no longer needed -->
		<dependency>
			<groupId>org.eclipse.sirius</groupId>
			<artifactId>sirius-components-collaborative-widget-reference</artifactId>
		</dependency>
		<dependency>
			<groupId>org.eclipse.sirius</groupId>
			<artifactId>sirius-web-services</artifactId>
		</dependency>

		<dependency>
			<groupId>org.eclipse.sirius</groupId>
			<artifactId>sirius-components-tests</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.eclipse.uml2.plugins</groupId>
			<artifactId>org.eclipse.uml2.uml.resources</artifactId>
		</dependency>
		<dependency>
			<groupId>org.eclipse.uml2.plugins</groupId>
			<artifactId>org.eclipse.uml2.common</artifactId>
		</dependency>
		<dependency>
			<groupId>org.eclipse.uml2.plugins</groupId>
			<artifactId>org.eclipse.uml2.types</artifactId>
		</dependency>
		<dependency>
			<groupId>org.eclipse.uml2.plugins</groupId>
			<artifactId>org.eclipse.uml2.uml.profile.standard</artifactId>
		</dependency>
		<dependency>
			<groupId>org.eclipse.emf</groupId>
			<artifactId>org.eclipse.emf.mapping.ecore2xml</artifactId>
		</dependency>
		<dependency>
			<groupId>papyrus-uml-domain-services</groupId>
			<artifactId>org.eclipse.papyrus.uml.domain.services</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.osgi</groupId>
					<artifactId>org.osgi.service.prefs</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
	</dependencies>

</project>
